package com.hfkj.cache.aspect.bo;

import org.springframework.beans.BeanUtils;

public class KeyPageBo<K, V> extends PageBo<V> implements IPageBo<V> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8154352438639925025L;
	/***
	 * 缓存中KEY值
	 */
	private K key;


	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public PageBo<V> convertToPageBo(){
		PageBo<V> pageBo=new PageBo<V>();
		BeanUtils.copyProperties(this, pageBo);
		return pageBo;
	}

}
