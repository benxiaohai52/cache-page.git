package com.hfkj.cache.aspect.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 缓存参数对象
 * 
 * @author hec
 *
 * @param <P>条件类
 * @param <T>返回数据类
 * @param <K>KEY对象类
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class DefaultPageBo<K, V, T> extends KeyPageBo<K, V> implements IParamPageBo<T,K> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private T condition;


	

	@Override
	public T getCondition() {
		// TODO Auto-generated method stub
		return this.condition;
	}

	@Override
	public void setCondition(T condition) {
		// TODO Auto-generated method stub
		this.condition = condition;
	}

}
