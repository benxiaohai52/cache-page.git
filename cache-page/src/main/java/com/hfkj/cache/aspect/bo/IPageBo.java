package com.hfkj.cache.aspect.bo;

import java.util.List;

public interface IPageBo<T> {

	/**
	 * 获取返回数据类
	 * 
	 * @return
	 */
	default Class<?> getResponseClass() {
		return null;
	}

	/**
	 * 获取开始下标
	 * 
	 * @return
	 */
	default long getIndexStart() {
		return getCurrent() * getSize();
	}

	/**
	 * 获取结束下标
	 * 
	 * @return
	 */
	default long getIndexEnd() {
		// TODO Auto-generated method stub
		return getCurrent() * getSize() + getSize() - 1;
	}

	public long getTotal();

	public void setTotal(long total);

	public long getSize();

	public void setSize(long size);

	public long getCurrent();

	public void setCurrent(long current);

	public List<T> getRecords();

	public void setRecords(List<T> records);
}
