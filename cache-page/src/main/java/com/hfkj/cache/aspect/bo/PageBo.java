package com.hfkj.cache.aspect.bo;

import java.io.Serializable;
import java.util.List;

public class PageBo<T> implements Serializable, IPageBo<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8681804565168106013L;

	/**
	 * 总数
	 */
	private long total = 0;
	/**
	 * 每页显示条数，默认 10
	 */
	private long size = 10;
	/**
	 * 当前页
	 */
	private long current = 1;

	/**
	 * 分页数据
	 */
	private List<T> records;

	@Override
	public long getTotal() {
		return total;
	}

	@Override
	public void setTotal(long total) {
		this.total = total;
	}

	@Override
	public long getSize() {
		return size;
	}

	@Override
	public void setSize(long size) {
		this.size = size;
	}

	@Override
	public long getCurrent() {
		return current;
	}

	@Override
	public void setCurrent(long current) {
		this.current = current;
	}

	@Override
	public List<T> getRecords() {
		return records;
	}

	@Override
	public void setRecords(List<T> records) {
		this.records = records;
	}
}
