package com.hfkj.cache.aspect.bo;

public interface IKeyPageBo<K> {
	K getKey();

	void setKey(K key);
}
