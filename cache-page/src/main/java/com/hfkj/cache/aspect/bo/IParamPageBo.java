package com.hfkj.cache.aspect.bo;

public interface IParamPageBo<T,K> extends IKeyPageBo<K> {
	/**
	 * 获取查询条件
	 * @return
	 */
	T getCondition();

	/**
	 * 设置查询条件
	 * @param condition
	 */
	void setCondition(T condition);
	
	
	/**
	 * 刷新（缓存刷新）触发条件，默认不刷新
	 * 如果刷新需重写此方法
	 * @return
	 */
	default Boolean isRefresh() {
		return false;
	}
	
}
