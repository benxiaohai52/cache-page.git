package com.hfkj.cache.aspect;

import java.lang.reflect.Method;

import javax.annotation.Resource;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.alibaba.fastjson.JSON;
import com.hfkj.cache.aspect.bo.DefaultPageBo;
import com.hfkj.cache.aspect.bo.PageBo;
import com.hfkj.cache.key.IKey;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class CacheAspect {

	/***
	 * 缓存数据最大长度，默认100条
	 */
	private int size = 100;

	private final String POINT_CUT = "@annotation(com.hfkj.cache.aspect.CacheAnnotation)";

	/***
	 * 目前通过redis去缓存数据；后期如果用其它方式只需要实现接口即可，业务代码不必修改
	 */
	@Resource(name = "redisKeyImpl")
	private IKey<String, Object> keyImpl;

	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Around(value = POINT_CUT)
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		// 获取参数
		DefaultPageBo args = getArgs(pjp);

		if (ObjectUtils.isEmpty(args)) {// 如果参数不传入则不执行缓存操作
			if (log.isDebugEnabled()) {
				log.debug("参数为空，暂时缓存不支持参数为空处理");
			}
			return pjp.proceed();
		}

		// 如果没有设置KEY，则使用系统默认的生成策略
		if (ObjectUtils.isEmpty(args.getKey())) {
			args.setKey(createKey(getClassName(pjp), getMethodName(pjp), args.getCondition()));
		}

		String key = args.getKey().toString();
		// 就否需要
		if (args.isRefresh()) {
			keyImpl.delete(key);
		}

		//获取缓存过期时间
		long expireTime=getExprieTime(pjp);
		if (keyImpl.keyExist(key)) {// 缓存获取数据
			if (log.isDebugEnabled()) {
				log.debug("通过缓存取数据；KEY：" + args.getKey());
			}
			// 更新key过期时间
			keyImpl.renewExpireTime(key, expireTime);
		} else {// 数据库获取数据
				// 执行方法体
			Object retVal = pjp.proceed();
			/**
			 * 是否按约定返回数据，如果不按约定则不进行缓存处理
			 */
			if (retVal instanceof PageBo) {
				PageBo pageBo = (PageBo) retVal;
				if (ObjectUtils.isEmpty(pageBo.getRecords()) || pageBo.getRecords().size() == 0) {
					return pageBo;
				}

				keyImpl.addListToList(key, pageBo.getRecords());
				keyImpl.expireTime(key, expireTime);
			} else {// 未查询到数据不必缓存
				return retVal;
			}

		}
		return keyImpl.listPage(args).convertToPageBo();
	}

	@SuppressWarnings("rawtypes")
	private DefaultPageBo getArgs(ProceedingJoinPoint pjp) {
		// 获取参数值
		Object[] args = pjp.getArgs();
		for (int i = 0; i < args.length; i++) {
			if (args[i] instanceof DefaultPageBo) {
				return (DefaultPageBo) args[i];
			}
		}
		return null;
	}

	/**
	 * 暂时不用，因为考虑在这里做限制不合适，入侵业务校验
	 * 
	 * @param args
	 * @return
	 */
	@SuppressWarnings({ "unused", "rawtypes" })
	private boolean legalityCheck(DefaultPageBo args) {
		// 计算请求尾数记录坐标
		return args.getIndexEnd() > size;
	}

	/**
	 * 获取返回值类型
	 * 
	 * @param pjp
	 * @return
	 */
	@SuppressWarnings("unused")
	private Class<?> getReturnType(ProceedingJoinPoint pjp) {
		Signature signature = pjp.getSignature();
		Class<?> returnType = ((MethodSignature) signature).getReturnType();
		return returnType;
	}

	private String getClassName(ProceedingJoinPoint pjp) {

		return pjp.getTarget().getClass().getName().replace(".", ":");
	}

	private String getMethodName(ProceedingJoinPoint pjp) {
		return pjp.getSignature().getName().replace(".", ":");
	}

	/**
	 * KEY默认生成策略，类全路径+方法名称+条件
	 * 
	 * @param className
	 * @param methodName
	 * @return
	 */
	private String createKey(String className, String methodName, Object o) {
		StringBuilder sb = new StringBuilder();
		sb.append(className);
		sb.append(":" + methodName);
		sb.append(JSON.toJSONString(o));
		return sb.toString();
	}
	
	private long getExprieTime(ProceedingJoinPoint pjp) {
		return getCacheAnnotation(pjp).expireTime();
	}

	private CacheAnnotation getCacheAnnotation(ProceedingJoinPoint pjp) {
		return getMethod(pjp).getAnnotation(CacheAnnotation.class);
	}

	/***
	 * 
	 * @Title: getMethod @Description: TODO(获取被拦截的方法) @author 侯恩呈 @param @param
	 *         jp @param @return 设定文件 @return Method 返回类型 @throws
	 */
	private Method getMethod(ProceedingJoinPoint pjp) {
		MethodSignature joinPointObject = (MethodSignature) pjp.getSignature();
		return joinPointObject.getMethod();
	}

}
