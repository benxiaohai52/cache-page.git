package com.hfkj.cache.test;

import lombok.Data;

@Data
public class ResultBo {
	
	private int id;
	private String name;
	private String phone;

}
