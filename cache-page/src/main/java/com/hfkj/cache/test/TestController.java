package com.hfkj.cache.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hfkj.cache.aspect.bo.DefaultPageBo;
import com.hfkj.cache.aspect.bo.PageBo;

/**
 * 
 * @author hec
 *
 */
@RequestMapping("/page")
@RestController
public class TestController {
	
	@Autowired
	private TestPageService pageService;
	
	@RequestMapping("/list")
	public PageBo<ResultBo>  listPage(){
		DefaultPageBo<String, ResultBo, ParamBo> cachePageBo = new DefaultPageBo<String, ResultBo, ParamBo>();
		ParamBo paramBo = new ParamBo();
		paramBo.setName("test name");
		paramBo.setPhone("test phone");
		cachePageBo.setCondition(paramBo);
		cachePageBo.setCurrent(0L);
		cachePageBo.setSize(20L);
		 PageBo<ResultBo> list = pageService.list(cachePageBo);

		 return list;
	}

	
}
