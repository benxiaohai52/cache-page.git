package com.hfkj.cache.test;

import java.util.ArrayList;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.hfkj.cache.aspect.CacheAnnotation;
import com.hfkj.cache.aspect.bo.DefaultPageBo;
import com.hfkj.cache.aspect.bo.PageBo;

@Service
public class TestPageService {
	
	
	@CacheAnnotation
	public PageBo<ResultBo>  list(DefaultPageBo<String, ResultBo, ParamBo> cp){
		
		cp.setRecords(new ArrayList<ResultBo>());
		for (int i = 0; i < 30; i++) {
			ResultBo resultBo = new ResultBo();
			resultBo.setId(i);
			resultBo.setName("name:"+i);
			resultBo.setPhone("phone:"+i);
			cp.getRecords().add(resultBo);
		}
		
		return cp;
	}

}
