package com.hfkj.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CachePageApplication {

	public static void main(String[] args) {
		SpringApplication.run(CachePageApplication.class, args);
	}

}
