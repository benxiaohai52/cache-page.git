package com.hfkj.cache.key;

import java.util.List;
import com.hfkj.cache.aspect.bo.KeyPageBo;

/***
 * KEY处理接口
 * 
 * @author hec
 *
 */
public interface IKey<K, V> {
	/***
	 * 校验KEY是否存在；存在则返回true否则返回false
	 * 
	 * @param key
	 * @return
	 */
	boolean keyExist(K key);

	/***
	 * 更新KEY过期时间
	 * 
	 * @param key  指定KEY
	 * @param time 单位秒
	 * @return
	 */
	boolean renewExpireTime(K key, long time);

	/***
	 * 设置KEY过期时间
	 * 
	 * @param key  指定KEY
	 * @param time 单位秒
	 * @return
	 */
	boolean expireTime(K key, long time);

	/***
	 * 根据KEY获取集合中指定区间数据
	 * @param <P>
	 * 
	 * @param keyPageBo
	 * @return
	 */
	KeyPageBo<K, V> listPage(KeyPageBo<K, V> keyPageBo);

	/**
	 * list数据存入缓存LIST集合中
	 * 
	 * @param key
	 * @param list
	 * @return
	 */
	Long addListToList(K key, List<V> list);
	
	/***
	 * 删除指定KEY
	 * @param key
	 * @return
	 */
	Boolean delete(K key);
}
