package com.hfkj.cache.key;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import com.hfkj.cache.aspect.bo.KeyPageBo;

@Component
public class RedisKeyImpl implements IKey<String, Object> {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	
	@Override
	public boolean keyExist(String key) {
		// TODO Auto-generated method stub

		return redisTemplate.hasKey(key);
	}

	@Override
	public boolean renewExpireTime(String key, long time) {
		// TODO Auto-generated method stub
		return redisTemplate.expire(key, time, TimeUnit.SECONDS);
	}

	@Override
	public boolean expireTime(String key, long time) {
		// TODO Auto-generated method stub
		return redisTemplate.expire(key, time, TimeUnit.SECONDS);
	}

	@Override
	public Long addListToList(String key, List<Object> list) {
		// TODO Auto-generated method stub
		return redisTemplate.opsForList().rightPushAll(key, list);
	}

	@Override
	public KeyPageBo<String, Object> listPage(KeyPageBo<String, Object> keyPageBo) {
		// TODO Auto-generated method stub
		List<Object> list = redisTemplate.opsForList().range(keyPageBo.getKey(), keyPageBo.getIndexStart(),
				keyPageBo.getIndexEnd());
		keyPageBo.setRecords(list);
		Long zCard = redisTemplate.opsForList().size(keyPageBo.getKey());
		keyPageBo.setTotal(zCard);
		return keyPageBo;
	}

	@Override
	public Boolean delete(String key) {
		// TODO Auto-generated method stub
		return redisTemplate.delete(key);
	}

	

}
